<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Section;

?>

<div class="card bd-primary">
	<div class="card-header bg-primary tx-white"><?= $this->title ?></div>
	<div class="card-body pd-sm-30">

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'full_name')->textInput() ?>
		<?= $form->field($model, 'username')->textInput() ?>
		<?= $form->field($model, 'email')->textInput() ?>
		<?php if ($model->isNewRecord): ?>
			<?= $form->field($model, 'password')->passwordInput() ?>
		<?php endif ?>
		<?= $form->field($model, 'is_admin')->dropDownList([
			'0' => 'Нет',
			'1' => 'Да',
			]) ?>


		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-teal' : 'btn btn-indigo']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>
</div>
