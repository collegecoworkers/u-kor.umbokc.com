<?php

use yii\helpers\Html;

$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
	'model' => $model,
]) ?>