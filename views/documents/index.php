<?php
use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Section;
use app\models\Documents;
use app\models\Copy;

?>
<p>
	<?= Html::a($this->params['add_label'], ['create'], ['class' => 'btn btn-pink']) ?>
</p>

<div class="card bd-primary mg-t-20">
	<div class="card-header bg-primary tx-white"><?= $this->title ?></div>
	<div class="card-body pd-sm-30">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'summary'=>'',
			'columns' => [
				[
					'class' => \yii\grid\SerialColumn::class,
					'headerOptions' => ['style' => 'width:70px'],
				],
				'name',
				'desc',
				[
					'attribute' => 'section',
					'format' => 'raw',
					'value' => function($data){
						return Section::findOne($data->section)->name;
					},
					'filter' => Html::activeDropDownList($searchModel, 'section', Section::getAllMap(), ['prompt'=>'Все', 'class' => 'form-control']),
				],
				[
					'attribute' => 'in_stock',
					'format' => 'raw',
					'value' => function($data){
						return Documents::theStockState($data->in_stock);
					},
					'filter' => Html::activeDropDownList($searchModel, 'in_stock', Documents::inStockStates(), ['prompt'=>'Все', 'class' => 'form-control']),
				],
				[
					'label' => 'Выданно копий',
					'format' => 'raw',
					'value' => function($data){
						return  Copy::find()->where(['document' => $data->id])->count();
					},
					// 'filter' => Html::activeDropDownList($searchModel, 'section', Section::getAllMap(), ['prompt'=>'Все', 'class' => 'form-control']),
				],
				[
					'attribute' => 'file',
					'format' => 'raw',
					'value' => function($data){
						return $data->file ? Html::a('Скачать', ['download', 'id' => $data->id]) : 'Нет файла';
					},
					'filter' => Html::activeDropDownList($searchModel, 'file', ['0' => 'Нет', '1' => 'Есть',], ['prompt'=>'Все', 'class' => 'form-control']),
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'header'=>'Действия', 
					'headerOptions' => ['width' => '80'],
					'template' => '{update} {delete}',
					'buttons' => [
						'update' => function ($url, $model, $key) {
							return Html::a('<i class="ion-edit"></i>', $url);
						},
						'delete' => function ($url, $model, $key) {
							return Html::a(
								'<i class="ion-android-delete" style="font-size: 115%;"></i>',
								$url,
								[
									'data' => [
										'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
									]
								]
							);
						},
					],
				],
			],
		]); ?>
	</div>
</div>