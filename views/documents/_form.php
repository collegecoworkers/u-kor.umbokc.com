<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\Section;

?>

<div class="card bd-primary">
	<div class="card-header bg-primary tx-white"><?= $this->title ?></div>
	<div class="card-body pd-sm-30">

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'name')->textInput() ?>
		<?= $form->field($model, 'desc')->textArea() ?>
		<?= $form->field($model, 'section')->dropDownList(Section::getAllMap()) ?>

		<?php if ($model->isNewRecord): ?>
			<?= $form->field($file, 'file')->fileInput() ?>
		<?php endif ?>

		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-teal' : 'btn btn-indigo']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>
</div>
