<?php
use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Documents;

?>
<p>
	<?= Html::a($this->params['add_label'], ['create'], ['class' => 'btn btn-pink']) ?>
</p>

<div class="card bd-primary mg-t-20">
	<div class="card-header bg-primary tx-white"><?= $this->title ?></div>
	<div class="card-body pd-sm-30">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'summary'=>'',
			'columns' => [
				[
					'class' => \yii\grid\SerialColumn::class,
					'headerOptions' => ['style' => 'width:70px'],
				],
				'name',
				[
					'label' => 'Кол-во документов',
					'headerOptions' => ['style' => 'width:70px'],
					'format' => 'raw',
					'value' => function($data){
						return Html::a(Documents::sectionCount($data->id), ['/documents', 'Documents[section]' => $data->id]);
					}
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'header'=>'Действия', 
					'headerOptions' => ['width' => '80'],
					'template' => '{update} {delete}',
					'buttons' => [
						'update' => function ($url, $model, $key) {
							return Html::a('<i class="ion-edit"></i>', $url);
						},
						'delete' => function ($url, $model, $key) {
							return Html::a(
								'<i class="ion-android-delete" style="font-size: 115%;"></i>',
								$url,
								[
									'data' => [
										'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
									]
								]
							);
						},
					],
				],
			],
		]); ?>
	</div>
</div>