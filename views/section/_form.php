<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="card bd-primary">
	<div class="card-header bg-primary tx-white"><?= $this->title ?></div>
	<div class="card-body pd-sm-30">

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'name')->textInput() ?>

		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-teal' : 'btn btn-indigo']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>
</div>
