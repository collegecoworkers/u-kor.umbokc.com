<?php
use yii\helpers\Html;

// dbg(Yii::$app->controller);

$menu = [];
$menu[] = [
	'label' => 'Секции',
	'url' => 'section',
	'icon' => 'ios-folder-outline',
];
$menu[] = [
	'label' => 'Документы',
	'url' => 'documents',
	'icon' => 'ios-book-outline',
];
$menu[] = [
	'label' => 'Выдача копий',
	'url' => 'copy',
	'icon' => 'ios-photos-outline',
];
$menu[] = [
	'label' => 'Выдача документов',
	'url' => 'transfer',
	'icon' => 'ios-albums-outline',
];
$menu[] = [
	'label' => 'Сотрудники',
	'url' => 'users',
	'icon' => 'person-stalker',
];

?>

<div class="sh-headpanel" style="left: 240px;">
	<div class="sh-headpanel-left">

		<?php foreach ($menu as $item): ?>
			<a style="width: 150px" href="/<?= $item['url'] ?>" class="sh-icon-link" <?=  Yii::$app->controller->id == $item['url'] ? 'c#f' : '' ?>>
				<div>
					<i class="icon ion-<?= $item['icon'] ?>"></i>
					<span><?= $item['label'] ?></span>
				</div>
			</a>
		<?php endforeach ?>

	</div><!-- sh-headpanel-left -->
	<div class="sh-headpanel-right">
		<div class="dropdown mg-r-10">
			<a href="/site/logout" class="dropdown-link dropdown-link-notification">
				<i class="icon ion-log-out tx-24"></i>
			</a>
		</div>
	</div><!-- sh-headpanel-right -->
</div><!-- sh-headpanel -->
