<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

if (Yii::$app->controller->action->id == 'login' || Yii::$app->controller->action->id == 'signup') { 
	echo $this->render(
		'main-login',
		['content' => $content]
	);
} else {


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">

	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

</head>
<body class="hide-left">
<?php $this->beginBody() ?>

<div class="sh-logopanel" style="left: 0;">
	<a href="/" class="sh-logo-text">Архивариус</a>
</div><!-- sh-logopanel -->

<?= $this->render('header.php', ['directoryAsset' => $directoryAsset]) ?>
<?= $this->render('left.php', ['directoryAsset' => $directoryAsset]) ?>
<div class="sh-mainpanel">
	<div class="sh-breadcrumb">
		<nav class="breadcrumb">
			<a class="breadcrumb-item" href="/">Главная</a>
			<?php $count_breadcrumbs = count($this->params['breadcrumbs']); $i = 0; ?>
			<?php foreach ($this->params['breadcrumbs'] as $item): ?>
			<?php ++$i; ?>
				<span class="breadcrumb-item <?= $count_breadcrumbs == $i ? 'active' : '' ?>"><?= $item ?></span>
			<?php endforeach ?>
		</nav>
	</div><!-- sh-breadcrumb -->
	<div class="sh-pagetitle">
		<div></div>
		<div class="sh-pagetitle-left">
			<div class="sh-pagetitle-icon"><i class="icon ion-<?= $this->params['icon'] ? $this->params['icon'] : 'ios-home' ?>"></i></div>
			<div class="sh-pagetitle-title">
				<h2><?= $this->title ?></h2>
			</div><!-- sh-pagetitle-left-title -->
		</div><!-- sh-pagetitle-left -->
	</div><!-- sh-pagetitle -->

	<div class="sh-pagebody">
		<?= $content ?>
	</div>
	<?= $this->render('footer.php', ['directoryAsset' => $directoryAsset]) ?>
	</div><!-- sh-mainpanel -->
</body>
</html>
<?php $this->endBody() ?>
<?php $this->endPage() ?>

<?php } ?>
