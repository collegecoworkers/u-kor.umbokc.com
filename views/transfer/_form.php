<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\User;
use app\models\Documents;
use app\models\Transfer;

?>

<div class="card bd-primary">
	<div class="card-header bg-primary tx-white"><?= $this->title ?></div>
	<div class="card-body pd-sm-30">

		<?php $form = ActiveForm::begin(); ?>

		<?php if ($model->isNewRecord): ?>
			<?= $form->field($model, 'document')->dropDownList(Documents::getAllMapInStock()) ?>
		<?php else: ?>
			<?= $form->field($model, 'document')->dropDownList(Documents::getAllMap()) ?>
		<?php endif ?>
		<?= $form->field($model, 'user')->dropDownList(User::getAllMap()) ?>

		<?php if (!$model->isNewRecord): ?>
			<?= $form->field($model, 'returned')->dropDownList(Transfer::returnedStatuses()) ?>
		<?php endif ?>

		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? 'Выдать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-teal' : 'btn btn-indigo']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>
</div>
