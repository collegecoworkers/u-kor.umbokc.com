<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';

?>
<?php $form = ActiveForm::begin(); ?>
<div class="signpanel-wrapper">
	<div class="signbox">
		<div class="signbox-header">
			<h2>Авторизация</h2>
		</div><!-- signbox-header -->
		<div class="signbox-body">
			<div class="form-group">
				<label class="form-control-label">Email:</label>
				<?= $form
				->field($model, 'email')
				->label(false)
				->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>
			</div><!-- form-group -->
			<div class="form-group">
				<label class="form-control-label">Пароль:</label>
				<?= $form
				->field($model, 'password')
				->label(false)
				->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>
			</div><!-- form-group -->
			<button class="btn btn-success btn-block">Войти</button>
			<div class="tx-center bg-white bd pd-10 mg-t-40">Еще не зарегистрированны? <a href="/site/signup">Создать учетную запись</a></div>
			<?php ActiveForm::end(); ?>
		</div><!-- signbox-body -->
	</div><!-- signbox -->
</div><!-- signpanel-wrapper -->
