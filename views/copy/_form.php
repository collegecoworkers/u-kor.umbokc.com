<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use app\models\User;
use app\models\Documents;

?>

<div class="card bd-primary">
	<div class="card-header bg-primary tx-white"><?= $this->title ?></div>
	<div class="card-body pd-sm-30">

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'document')->dropDownList(Documents::getAllMap()) ?>
		<?= $form->field($model, 'user')->dropDownList(User::getAllMap()) ?>

		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? 'Выдать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-teal' : 'btn btn-indigo']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>
</div>
