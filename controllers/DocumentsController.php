<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;

use app\models\Documents;
use app\models\FileUpload;

class DocumentsController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['*',],
				'rules' => [
					[
						'allow' => true,
						'verbs' => ['POST']
					],
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{
		Yii::$app->view->params['add_label'] = 'Внести документ';
		$this->setParams('Документы', 'ios-book');

		$searchModel = new Documents();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate()
	{

		$this->setParams('Новый документ', 'ios-book');

		$model = new Documents();
		$file = new FileUpload;

		if ($model->load(Yii::$app->request->post())) {
			if(UploadedFile::getInstance($file, 'file'))
				$model->file = $file->uploadFile(UploadedFile::getInstance($file, 'file'));
			$model->save();
			return $this->redirect(['index']);
		} else {
			return $this->render('create', [
				'model' => $model,
				'file' => $file,
			]);
		}
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$this->setParams($model->name, 'ios-book');
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	public function actionDownload($id)
	{
		$file = $this->findModel($id);
		$uf = new FileUpload;
		return $uf->downloadFile($file->file, $file->name);
	}

	protected function findModel($id)
	{
		if (($model = Documents::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function setParams($title, $icon = null){
		Yii::$app->view->title = $title;
		Yii::$app->view->params['breadcrumbs'][] = $title;
		if ($icon) Yii::$app->view->params['icon'] = $icon;
	}
}
