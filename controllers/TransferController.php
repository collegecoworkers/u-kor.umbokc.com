<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;

use app\models\Transfer;
use app\models\Documents;

class TransferController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['*',],
				'rules' => [
					[
						'allow' => true,
						'verbs' => ['POST']
					],
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{

		Yii::$app->view->params['add_label'] = 'Выдать документ';
		$this->setParams('Выдача докуентов', 'ios-albums');

		$searchModel = new Transfer();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate()
	{
		$this->setParams('Выдать документ', 'ios-albums');
		$model = new Transfer();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$doc = Documents::findOne($model->document);
			$doc->in_stock = false;
			$doc->save();
			return $this->redirect(['index']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$this->setParams('Изменить данные', 'ios-albums');

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$doc = Documents::findOne($model->document);
			$doc->in_stock = $model->returned;
			$doc->save();
			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	protected function findModel($id)
	{
		if (($model = Transfer::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function setParams($title, $icon = null){
		Yii::$app->view->title = $title;
		Yii::$app->view->params['breadcrumbs'][] = $title;
		if ($icon) Yii::$app->view->params['icon'] = $icon;
	}
}
