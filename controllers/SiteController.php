<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;

use app\models\LoginForm;
use app\models\SignupForm;

class SiteController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['index',],
				'rules' => [
					[
						'allow' => true,
						'verbs' => ['POST']
					],
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{
		if ( Yii::$app->user->isGuest )
			return $this->redirect('login');

		return $this->redirect('section');
	}

	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}
		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionSignup()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$model = new SignupForm();

		if(Yii::$app->request->isPost)
		{
			$model->load(Yii::$app->request->post());
			if($model->signup())
			{
				return $this->redirect(['/site/login']);
			}
		}

		return $this->render('signup', ['model'=>$model]);
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}
}
