<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'public/lib/font-awesome/css/font-awesome.css',
		'public/lib/Ionicons/css/ionicons.css',
		'public/lib/perfect-scrollbar/css/perfect-scrollbar.css',
		'public/css/shamcey.css',
		'css/site.css',
	];
	public $js = [
		// 'public/lib/jquery/jquery.js',
		'public/lib/popper.js/popper.js',
		'public/lib/bootstrap/bootstrap.js',
		'public/lib/jquery-ui/jquery-ui.js',
		'public/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js',
		'public/lib/moment/moment.js',
		// 'public/lib/Flot/jquery.flot.js',
		// 'public/lib/Flot/jquery.flot.resize.js',
		// 'public/lib/flot-spline/jquery.flot.spline.js',

		'public/js/shamcey.js',
		'public/js/dashboard.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		// 'yii\bootstrap\BootstrapAsset',
	];
}
