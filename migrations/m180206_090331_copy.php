<?php

use yii\db\Migration;

class m180206_090331_copy extends Migration
{

	public function up()
	{
		$this->createTable('copy', [
			'id' => $this->primaryKey(),
			'user'=>$this->integer(),
			'document'=>$this->integer(),
		]);
	}

	public function down()
	{
		$this->dropTable('copy');
	}
}
