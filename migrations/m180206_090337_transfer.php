<?php

use yii\db\Migration;

class m180206_090337_transfer extends Migration
{

	public function up()
	{
		$this->createTable('transfer', [
			'id' => $this->primaryKey(),
			'user'=>$this->integer(),
			'document'=>$this->integer(),
			'returned'=>$this->boolean()->defaultValue(0),
		]);
	}

	public function down()
	{
		$this->dropTable('transfer');
	}
}
