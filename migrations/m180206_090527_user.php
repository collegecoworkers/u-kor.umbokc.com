<?php

use yii\db\Migration;

class m180206_090527_user extends Migration
{

	public function up()
	{
		$this->createTable('user', [
			'id' => $this->primaryKey(),
			'full_name'=>$this->string(),
			'email'=>$this->string()->defaultValue(null),
			'username'=>$this->string(),
			'password'=>$this->string(),
			'is_admin'=>$this->integer()->defaultValue(0),
		]);
	}

	public function down()
	{
		$this->dropTable('user');
	}
}
