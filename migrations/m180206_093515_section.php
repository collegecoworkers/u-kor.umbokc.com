<?php

use yii\db\Migration;

class m180206_093515_section extends Migration
{

	public function up()
	{
		$this->createTable('section', [
			'id' => $this->primaryKey(),
			'name'=>$this->string(),
		]);
	}

	public function down()
	{
		$this->dropTable('section');
	}
}
