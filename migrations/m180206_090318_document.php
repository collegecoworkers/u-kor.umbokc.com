<?php

use yii\db\Migration;

class m180206_090318_document extends Migration
{

	public function up()
	{
		$this->createTable('document', [
			'id' => $this->primaryKey(),
			'name'=>$this->string(),
			'desc'=>$this->text(),
			'section'=>$this->integer(),
			'in_stock'=>$this->boolean()->defaultValue(1),
		]);
	}

	public function down()
	{
		$this->dropTable('document');
	}
}
