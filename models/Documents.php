<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class Documents extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'document';
	}

	public function rules()
	{
		return [
			[['name', 'desc', 'file', ], 'string'],
			[['section', ], 'integer'],
			[['in_stock', ], 'boolean'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
			'desc' => 'Описание',
			'section' => 'Секция',
			'in_stock' => 'В наличии',
			'file' => 'Файл',
		];
	}
	
	public static function findIdentity($id)
	{
		return self::findOne($id);
	}

	public static function getAllMap()
	{
		return ArrayHelper::map(self::find()->all(), 'id', 'name');
	}

	public static function getAllMapInStock()
	{
		return ArrayHelper::map(self::find()->where(['in_stock' => 1])->all(), 'id', 'name');
	}

	public static function sectionCount($id)
	{
		return self::find()->where(['section' => $id])->count();
	}

	public function scenarios()
	{
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = self::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if($params['Documents']['file'] == '1'){
			$query->andWhere(['not', ['file' => '']]);
		} elseif($params['Documents']['file'] == '0') {
			$query->andWhere(['file' => '']);
		}

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'name' => $this->name,
			'desc' => $this->desc,
			'section' => $this->section,
			'in_stock' => $this->in_stock,
		]);

		if(!$params) $query->orderBy(['id'=>SORT_DESC]);

		return $dataProvider;
	}

	public static function inStockStates(){
		return [
			'0' => 'Нет',
			'1' => 'Да',
		];
	}

	public static function theStockState($s){
		$ss = self::inStockStates();
		if(array_key_exists($s, $ss)) 
			return $ss[$s];
		return $ss[0];
	}
}
