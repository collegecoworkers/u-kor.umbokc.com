<?php

namespace app\models;

use yii\base\Model;

class SignupForm extends Model
{
	public $username;
	public $email;
	public $password;

	public function rules()
	{
		return [
			[['username','email','password'], 'required'],
			[['username'], 'string'],
			[['email'], 'email'],
			[['email'], 'unique', 'targetClass'=>'app\models\User', 'targetAttribute'=>'email']
		];
	}

	public function attributeLabels()
	{
		return [
			'username' => 'Логин',
			'password' => 'Пароль',
			'email' => 'Почта',
		];
	}

	public function signup()
	{
		if($this->validate())
		{
			$user = new User();
			$user->attributes = $this->attributes;
			return $user->save(false);
		}
	}
}