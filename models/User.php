<?php

namespace app\models;

use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

	public static function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return [
			[['username','full_name','password', ], 'string'],
			[['email', ], 'email'],
			[['is_admin', ], 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'username' => 'Логин',
			'email' => 'Почта',
			'is_admin' => 'Админ',
		];
	}

	public static function findIdentity($id)
	{
		return self::findOne($id);
	}

	public static function findIdentityByAccessToken($token, $type = null)
	{
		return null;
	}

	public static function getAllMap()
	{
		return ArrayHelper::map(self::find()->all(), 'id', 'username');
	}

	public static function findByEmail($email)
	{
		return self::find()->where(['email' => $email])->one();
	}

	public static function findByUsername($username)
	{
		return self::find()->where(['username' => $username])->one();
	}

	public function getId()
	{
		return $this->id;
	}

	public function getAuthKey()
	{
		return $this->authKey;
	}

	public function validateAuthKey($authKey)
	{
		return $this->authKey === $authKey;
	}

	public function validatePassword($password)
	{
		return $this->password === $password;
	}

	public function search($params)
	{
		$query = self::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'username' => $this->username,
			'email' => $this->email,
			'is_admin' => $this->is_admin,
		]);

		if(!$params) $query->orderBy(['id'=>SORT_DESC]);

		return $dataProvider;
	}
}
