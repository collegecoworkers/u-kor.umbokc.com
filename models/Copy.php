<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class Copy extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'copy';
	}

	public function rules()
	{
		return [
			[['document','user', ], 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'document' => 'Документ',
			'user' => 'Пользователь',
		];
	}
	
	public static function findIdentity($id)
	{
		return self::findOne($id);
	}


	public function scenarios()
	{
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = self::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'user' => $this->user,
			'document' => $this->document,
		]);

		if(!$params) $query->orderBy(['id'=>SORT_DESC]);

		return $dataProvider;
	}
}
