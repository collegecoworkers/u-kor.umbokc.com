<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class Transfer extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'transfer';
	}

	public function rules()
	{
		return [
			[['document', 'user', 'returned', ], 'integer'],
		];
	}

	public function attributeLabels()
	{
		return [
			'document' => 'Документ',
			'user' => 'Пользователь',
			'returned' => 'Возвращенно',
		];
	}
	
	public static function findIdentity($id)
	{
		return self::findOne($id);
	}


	public function scenarios()
	{
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = self::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'user' => $this->user,
			'document' => $this->document,
			'returned' => $this->returned,
		]);

		if(!$params) $query->orderBy(['id'=>SORT_DESC]);

		return $dataProvider;
	}

	public static function returnedStatuses(){
		return [
			'0' => 'Нет',
			'1' => 'Да',
		];
	}

	public static function theReturnedStatus($s){
		$ss = self::returnedStatuses();
		if(array_key_exists($s, $ss)) 
			return $ss[$s];
		return $ss[0];
	}

}
