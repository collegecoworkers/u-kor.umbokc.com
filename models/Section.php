<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class Section extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'section';
	}

	public function rules()
	{
		return [
			[['name', ], 'string'],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
		];
	}
	
	public static function findIdentity($id)
	{
		return self::findOne($id);
	}

	public static function getAllMap()
	{
		return ArrayHelper::map(self::find()->all(), 'id', 'name');
	}

	public function scenarios()
	{
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = self::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'name' => $this->name
		]);

		if(!$params) $query->orderBy(['id'=>SORT_DESC]);

		return $dataProvider;
	}
}
